"""Test MarkdownBasedSite"""
import pytest
import pathlib
from markdown_based_site.markdown_based_site import MarkdownBasedSite

SELF_FILE = pathlib.Path(__file__).resolve()
DATA_FOLDER = SELF_FILE.joinpath('../data').resolve()
SOURCE_FOLDER = DATA_FOLDER.joinpath('source').resolve()
MD_TEMPLATE_FILE = DATA_FOLDER.joinpath('templates/template.j2.html').resolve()
EXIST_FOLDER = SOURCE_FOLDER.joinpath('dir01').resolve()
EXIST_FILE = SOURCE_FOLDER.joinpath('index.md').resolve()

NOT_EXIST_FOLDER = SOURCE_FOLDER.joinpath('index').resolve()
NOT_EXIST_FILE = SOURCE_FOLDER.joinpath('dir01.md').resolve()
PARENT_NOT_EXIST_FOLDER = SOURCE_FOLDER.joinpath('index/dummy').resolve()


class Test__Init__:
    """Test constructor"""

    def test_do_not_raise(self):
        # given
        # when
        MarkdownBasedSite(
            source_folder=str(EXIST_FOLDER),
            dest_folder=str(NOT_EXIST_FOLDER),
            md_template_file=str(EXIST_FILE),
        )
        # then

    @pytest.mark.parametrize("testcase, path, expect", [
        ('non_str', 123, TypeError),
        ('exist_file', str(EXIST_FILE), ValueError),
        ('not_exist_folder', str(NOT_EXIST_FOLDER), ValueError),
    ])
    def test_raise_when_source_folder_is_wrong(
        self, testcase, path, expect
    ):
        # given
        with pytest.raises(expect):
            # when
            MarkdownBasedSite(
                source_folder=path,
                dest_folder=str(NOT_EXIST_FOLDER),
                md_template_file=str(EXIST_FILE),
            )
            # then

    @pytest.mark.parametrize("testcase, path, expect", [
        ('non_str', 123, TypeError),
        ('exist_file', str(EXIST_FILE), ValueError),
        ('exist_folder', str(EXIST_FOLDER), ValueError),
        ('parent_not_exist_folder', str(PARENT_NOT_EXIST_FOLDER), ValueError),
    ])
    def test_raise_when_dest_folder_is_wrong(
        self, testcase, path, expect
    ):
        # given
        with pytest.raises(expect):
            # when
            MarkdownBasedSite(
                source_folder=str(EXIST_FOLDER),
                dest_folder=path,
                md_template_file=str(EXIST_FILE),
            )
            # then

    @pytest.mark.parametrize("testcase, path, expect", [
        ('non_str', 123, TypeError),
        ('not_exist_file', str(NOT_EXIST_FILE), ValueError),
        ('exist_folder', str(EXIST_FOLDER), ValueError),
    ])
    def test_raise_when_md_template_file_is_wrong(
        self, testcase, path, expect
    ):
        # given
        with pytest.raises(expect):
            # when
            MarkdownBasedSite(
                source_folder=str(EXIST_FOLDER),
                dest_folder=str(NOT_EXIST_FOLDER),
                md_template_file=path,
            )
            # then


class TestPublish:
    """Test MarkdownBasedSite.publish"""

    def test_copies_directories_and_files(self, tmp_path):
        # given
        dest_folder = tmp_path.joinpath(
            'test_copies_directories_and_files'
        ).resolve()

        site = MarkdownBasedSite(
            source_folder=str(SOURCE_FOLDER),
            dest_folder=str(dest_folder),
            md_template_file=str(MD_TEMPLATE_FILE),
        )

        # when
        site.publish()
        # then
        source_path_list = [p for p in SOURCE_FOLDER.glob(pattern='**/*')]
        dest_path_list = [p for p in dest_folder.glob(pattern='**/*')]

        # 1. all files in source should be in dest
        source_name_list = [p.name for p in source_path_list]
        dest_name_list = [p.name for p in dest_path_list]
        assert set(source_name_list) <= set(dest_name_list)

        # 2. all md files in source should be in dest as html
        source_md_to_html_name_list = [
            p.with_suffix('.html').name for p in source_path_list
            if p.suffix == '.md'
        ]
        dest_html_name_list = [
            p.name for p in dest_path_list
            if p.suffix == '.html'
        ]
        assert set(source_md_to_html_name_list) <= set(dest_html_name_list)
