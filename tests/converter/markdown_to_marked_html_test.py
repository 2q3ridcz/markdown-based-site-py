"""Test MarkdownToMarkedHtml"""
import pytest
import pathlib
from markdown_based_site.pathlib.existing_file_path import ExistingFilePath
from markdown_based_site.converter.markdown_to_marked_html import MarkdownToMarkedHtml

SELF_FILE = pathlib.Path(__file__).resolve()
DATA_FOLDER = SELF_FILE.joinpath('../markdown_to_marked_html').resolve()
SOURCE_FOLDER = DATA_FOLDER.joinpath('source').resolve()
EXPECT_FOLDER = DATA_FOLDER.joinpath('expect').resolve()
MD_TEMPLATE_FILE = DATA_FOLDER.joinpath('templates/template.j2.html').resolve()
MD_FILE = SOURCE_FOLDER.joinpath('mermaid.js.org.md').resolve()
EXPECT_FILE = EXPECT_FOLDER.joinpath('mermaid.js.org.html').resolve()


class Test__Init__:
    """Test constructor"""

    def test_do_not_raise(self):
        # given
        # when
        MarkdownToMarkedHtml(
            text="# Test\n\ntesttesttest\n",
            title="title",
            template_file=ExistingFilePath(path=str(MD_TEMPLATE_FILE)),
        )
        # then


class TestConvert:
    """Test MarkdownToMarkedHtml.convert"""

    def test_can_convert(self):
        # given
        md = MD_FILE.read_text(encoding='utf_8')
        expect = EXPECT_FILE.read_text(encoding='utf_8')

        converter = MarkdownToMarkedHtml(
            text=md,
            title="title",
            template_file=ExistingFilePath(path=str(MD_TEMPLATE_FILE)),
        )

        # when
        html = converter.convert()
        # then
        # TEMP_OUT_FILE = EXPECT_FOLDER.joinpath('acctual.html').resolve()
        # TEMP_OUT_FILE.write_text(html, encoding='utf_8')
        assert html.splitlines() == expect.splitlines()
