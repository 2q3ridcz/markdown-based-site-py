# Mermaidのサンプル

## 謝辞

[Mermaid | Diagramming and charting tool](https://mermaid.js.org/) のサイトのサンプルを使用させていただいています。

## flowchart

[flowchart](https://mermaid.js.org/syntax/flowchart.html#subgraphs) より引用

```mermaid
flowchart TB
    c1-->a2
    subgraph ide1 [one]
    a1-->a2
    end
```

## sequenceDiagram

[sequenceDiagram](https://mermaid.js.org/syntax/sequenceDiagram.html) より引用

```mermaid
sequenceDiagram
    Alice->>John: Hello John, how are you?
    John-->>Alice: Great!
    Alice-)John: See you later!
```

## classDiagram

[classDiagram](https://mermaid.js.org/syntax/classDiagram.html) より引用

```mermaid
classDiagram
    note "From Duck till Zebra"
    Animal <|-- Duck
    note for Duck "can fly\ncan swim\ncan dive\ncan help in debugging"
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
        +String beakColor
        +swim()
        +quack()
    }
    class Fish{
        -int sizeInFeet
        -canEat()
    }
    class Zebra{
        +bool is_wild
        +run()
    }
```

## stateDiagram

[stateDiagram](https://mermaid.js.org/syntax/stateDiagram.html) より引用

```mermaid
stateDiagram-v2
    [*] --> Still
    Still --> [*]

    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]
```

## entityRelationshipDiagram

[entityRelationshipDiagram](https://mermaid.js.org/syntax/entityRelationshipDiagram.html) より引用

```mermaid
erDiagram
    CUSTOMER ||--o{ ORDER : places
    ORDER ||--|{ LINE-ITEM : contains
    CUSTOMER }|..|{ DELIVERY-ADDRESS : uses
```

## gantt

[gantt](https://mermaid.js.org/syntax/gantt.html) より引用

```mermaid
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
```
