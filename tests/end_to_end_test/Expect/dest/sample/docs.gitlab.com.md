# GitLab Flavored Markdownのサンプル

## 謝辞

[GitLab Flavored Markdown (GLFM) | GitLab](https://docs.gitlab.com/ee/user/markdown.html) のサイトのサンプルを使用させていただいています。

## 対応状況まとめ

対応できない文法がある (Checked with GitLab Docs 15.11)。ただし、ここでまとめた結果は、ブラウザの種類やバージョンなど外的要因により結果が変わる可能性があるため注意。

試験項目|成否|期待値|note
--|--|--|--
[Colors](#colors)|Ng|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#colors)
[Mermaid](#mermaid)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#mermaid)
[Emojis](#emojis)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#emojis)
[Inline diff](#inline-diff)|Ng|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#inline-diff)
[Math](#math)|Partially ng|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#inline-diff)|Use `$$`. (``$` `` causes warning on conversion, and will not be rendered as math)
[Task lists](#task-lists)|Partially ng|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#task-lists)|Cannot render inapplicable checkbox(`~`). Cannot render ordered list.
[Table of contents](#table-of-contents)|Ng|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#table-of-contents)
[Blockquotes](#blockquotes)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#blockquotes)
[Multiline blockquote](#multiline-blockquote)|Ng|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#multiline-blockquote)
[Code spans and blocks](#code-spans-and-blocks)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#code-spans-and-blocks)
[Colored code and syntax highlighting](#colored-code-and-syntax-highlighting)|Ng|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#colored-code-and-syntax-highlighting)|May support in the future...
[Emphasis](#emphasis)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#emphasis)
[Multiple underscores in words and mid-word emphasis](#multiple-underscores-in-words-and-mid-word-emphasis)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#multiple-underscores-in-words-and-mid-word-emphasis)
[Footnotes](#footnotes)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#footnotes)
[Horizontal Rule](#horizontal-rule)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#horizontal-rule)
[Inline HTML](#inline-html)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#inline-html)
[Collapsible section](#collapsible-section)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#collapsible-section)
[Line breaks](#line-breaks)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#line-breaks)
[Newlines](#newlines)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#newlines)
[Links](#links)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#links)|Set id(#~) in url in lower case. Replace space to '-'.
[Lists](#lists)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#lists)
[Superscripts / Subscripts](#superscripts--subscripts)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#superscripts--subscripts)
[Keyboard HTML tag](#keyboard-html-tag)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#keyboard-html-tag)
[Tables_Markdown](#tables_markdown)|Ok|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#markdown)
[Tables_JSON](#tables_json)|Ng|[GitLab Docs](https://docs.gitlab.com/ee/user/markdown.html#json)


## Render results

### Colors

- `#F00`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`

### Mermaid

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```

### Emojis

Sometimes you want to :monkey: around a bit and add some :star2: to your
:speech_balloon:. Well we have a gift for you:

:zap: You can use emoji anywhere GitLab Flavored Markdown is supported. :v:

You can use it to point out a :bug: or warn about :speak_no_evil: patches.
And if someone improves your really :snail: code, send them some :birthday:.
People :heart: you for that.

If you're new to this, don't be :fearful:. You can join the emoji :family:.
Just look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.webfx.com/tools/emoji-cheat-sheet/)
for a list of all supported emoji codes. :thumbsup:

### Inline diff

- {+ addition 1 +}
- [+ addition 2 +]
- {- deletion 3 -}
- [- deletion 4 -]

### Math

<!-- (commented out. Causes error on conversion) -->
<!-- This math is inline: $`a^2+b^2=c^2`$. -->

This math is on a separate line using a ```` ```math ```` block:

```math
a^2+b^2=c^2
```

This math is on a separate line using inline `$$`: $$a^2+b^2=c^2$$

This math is on a separate line using a `$$...$$` block:

$$
a^2+b^2=c^2
$$


### Task lists

- [x] Completed task
- [~] Inapplicable task
- [ ] Incomplete task
  - [x] Sub-task 1
  - [~] Sub-task 2
  - [ ] Sub-task 3

1. [x] Completed task
1. [~] Inapplicable task
1. [ ] Incomplete task
   1. [x] Sub-task 1
   1. [~] Sub-task 2
   1. [ ] Sub-task 3

### Table of contents

[[_TOC_]]

[[TOC]]

### Blockquotes

> Blockquotes help you emulate reply text.
> This line is part of the same quote.

Quote break.

> This very long line is still quoted properly when it wraps. Keep writing to make sure this line is long enough to actually wrap for everyone. You can also *add* **Markdown** into a blockquote.

### Multiline blockquote

>>>
If you paste a message from somewhere else

that spans multiple lines,

you can quote that without having to manually prepend `>` to every line!
>>>

### Code spans and blocks

Inline `code` has `back-ticks around` it.

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python code"
    print s
```

    Using 4 spaces
    is like using
    3-backtick fences.

~~~
Tildes are OK too.
~~~

### Colored code and syntax highlighting

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

```
No language indicated, so no syntax highlighting.
s = "No highlighting is shown for this line."
But let's throw in a <b>tag</b>.
```

### Emphasis

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with double **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

### Multiple underscores in words and mid-word emphasis

perform_complicated_task

do_this_and_do_that_and_another_thing

but_emphasis is_desired _here_

perform*complicated*task

do*this*and*do*that*and*another thing

### Footnotes

A footnote reference tag looks like this: [^1]

This reference tag is a mix of letters and numbers. [^footnote-42]

[^1]: This text is inside a footnote.

[^footnote-42]: This text is another footnote.

### Horizontal Rule

Three or more hyphens,

---

asterisks,

***

or underscores

___

### Inline HTML

<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML <em>tags</em> do <b>work</b>, in most cases.</dd>
</dl>

<dl>
  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML tags work, in most cases.</dd>

  <dt>Markdown in HTML</dt>
  <dd>

  Does *not* work **very** well. HTML tags work, in most cases.

  </dd>
</dl>

### Collapsible section

<p>
<details>
<summary>Click this to collapse/fold.</summary>

These details <em>remain</em> <strong>hidden</strong> until expanded.

<pre><code>PASTE LOGS HERE</code></pre>

</details>
</p>

<details>
<summary>

Click this to _collapse/fold._

</summary>

These details _remain_ **hidden** until expanded.

```
PASTE LOGS HERE
```

</details>

### Line breaks

Here's a line for us to start with.

This longer line is separated from the one above by two newlines, so it is a *separate paragraph*.

This line is also a separate paragraph, but...
These lines are only separated by single newlines,
so they *do not break* and just follow the previous lines
in the *same paragraph*.

### Newlines

First paragraph.
Another line in the same paragraph.
A third line in the same paragraph, but this time ending with two spaces.  
A new line directly under the first paragraph.

Second paragraph.
Another line, this time ending with a backslash.\
A new line due to the previous backslash.

### Links

- This line shows an [inline-style link](https://www.google.com)
- This line shows a [link to a repository file in the same directory](permissions.md)
- This line shows a [relative link to a file one directory higher](../index.md)
- This line shows a [link that also has title text](https://www.google.com "This link takes you to Google!")

Using header ID anchors:

- This line links to [a section on a different Markdown page, using a "#" and the header ID](permissions.md#project-features-permissions)
- This line links to [a different section on the same page, using a "#" and the header ID](#header-ids-and-links)

Using references:

- This line shows a [reference-style link, see below][Arbitrary case-insensitive reference text]
- You can [use numbers for reference-style link definitions, see below][1]
- Or leave it empty and use the [link text itself][], see below.

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org/en-US/
[1]: https://slashdot.org
[link text itself]: https://www.reddit.com

### Lists

1. First ordered list item
2. Another item
   - Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
   1. Ordered sub-list
   1. Next ordered sub-list item
4. And another item.

Unordered lists can:

- use
- minuses

They can also:

* use
* asterisks

They can even:

+ use
+ pluses

If a list item contains multiple paragraphs, each subsequent paragraph should be indented to the same level as the start of the list item text.

1. First ordered list item

   Second paragraph of first item.

1. Another item

### Superscripts / Subscripts

The formula for water is H<sub>2</sub>O
while the equation for the theory of relativity is E = mc<sup>2</sup>.

### Keyboard HTML tag

Press <kbd>Enter</kbd> to go to the next page.

### Tables_Markdown

| header 1 | header 2 | header 3 |
| ---      | ---      | ---      |
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It eventually wraps the text when the cell is too large for the display size. |
| cell 7   |          | cell 9   |

With alignment:

| Left Aligned | Centered | Right Aligned |
| :---         | :---:    | ---:          |
| Cell 1       | Cell 2   | Cell 3        |
| Cell 4       | Cell 5   | Cell 6        |

Use HTML formatting:

| Name | Details |
| ---  | ---     |
| Item1 | This text is on one line |
| Item2 | This item has:<br>- Multiple items<br>- That we want listed separately |

### Tables_JSON

```json:table
{
    "items" : [
      {"a": "11", "b": "22", "c": "33"}
    ]
}
```

You can use the filter attribute to render a table with content filtered dynamically by user input.

```json:table
{
    "fields" : [
        {"key": "a", "label": "AA"},
        {"key": "b", "label": "BB"},
        {"key": "c", "label": "CC"}
    ],
    "items" : [
      {"a": "11", "b": "22", "c": "33"},
      {"a": "211", "b": "222", "c": "233"}
    ],
    "filter" : true
}
```
