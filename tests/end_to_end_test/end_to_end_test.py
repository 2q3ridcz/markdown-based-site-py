import pathlib
from markdown_based_site.markdown_based_site import MarkdownBasedSite

SELF_FILE = pathlib.Path(__file__).resolve()


class TestEndToEnd:
    """End to end test"""

    def test_end_to_end(self, tmp_path):
        # given
        expect_folder = SELF_FILE.parent.joinpath('Expect').resolve()
        EXPECT_FOLDER = expect_folder.joinpath('dest')

        input_folder = SELF_FILE.parent.joinpath('Input').resolve()
        # output_folder = SELF_FILE.parent.joinpath('Output').resolve()
        output_folder = tmp_path.resolve()

        SOURCE_FOLDER = input_folder.joinpath('source')
        DEST_FOLDER = output_folder.joinpath('dest')
        MD_TEMPLATE_FILE = input_folder.joinpath('templates/template.j2.html')

        site = MarkdownBasedSite(
            source_folder=str(SOURCE_FOLDER),
            dest_folder=str(DEST_FOLDER),
            md_template_file=str(MD_TEMPLATE_FILE),
        )
        # when
        site.publish()
        # then
        acctual_files = [p for p in DEST_FOLDER.glob("**/*")]
        acctual_files.sort()
        expect_files = [p for p in EXPECT_FOLDER.glob("**/*")]
        expect_files.sort()

        assert len(acctual_files) == len(expect_files)
        for acctual_file, expect_file in zip(acctual_files, expect_files):
            assert acctual_file.is_file() == expect_file.is_file()
            if not acctual_file.is_file():
                continue

            assert (
                str(acctual_file.relative_to(DEST_FOLDER)) ==
                str(expect_file.relative_to(EXPECT_FOLDER))
            )
            assert (
                acctual_file.read_text(encoding='utf_8') ==
                expect_file.read_text(encoding='utf_8')
            )
