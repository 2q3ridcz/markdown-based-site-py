# サンプル

- [簡単な Markdown のサンプル](./sample/www.markdown.jp.md)
- [Markdown に Mermaid の図を組み込むサンプル](./sample/mermaid.js.org.md)
- [GitLab Flavored Markdownのサンプル](./sample/docs.gitlab.com.md)
