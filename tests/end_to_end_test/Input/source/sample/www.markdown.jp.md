# 日本語Markdownユーザー会のサンプル

## 謝辞

[日本語Markdownユーザー会](https://www.markdown.jp/) のサイトのサンプルを使用させていただいています。

## markdownの書き方

[markdownの書き方](https://www.markdown.jp/what-is-markdown/index.html#markdownの書き方) より引用

Markdown（マークダウン）は、**文章の書き方**です。デジタル文書を活用する方法として考案されました。特徴は、

- 手軽に文章構造を明示できること
- 簡単で、覚えやすいこと
- 読み書きに特別なアプリを必要としないこと
- それでいて、対応アプリを使えば快適に読み書きできること

などです。Markdownはジョン・グルーバー（John Gruber）によって2004年に開発され、最初は [Daring Fireball: Markdown](http://daringfireball.net/projects/markdown/) で公開されました。その後、多くの開発者の手を経ながら発展してきました。
