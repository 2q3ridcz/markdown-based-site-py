import pytest
import pathlib
from markdown_based_site.pathlib.existing_folder_path import ExistingFolderPath
from markdown_based_site.pathlib.not_existing_folder_path import NotExistingFolderPath

SELF_FILE = pathlib.Path(__file__).resolve()
DATA_FOLDER = SELF_FILE.joinpath('../../data').resolve()
SOURCE_FOLDER = DATA_FOLDER.joinpath('source').resolve()


class Test__Init__:
    """Test constructor"""
    def test_do_not_raise(self):
        # given
        # when
        ExistingFolderPath(path=str(SOURCE_FOLDER))
        # then

    def test_raise_when_non_str_is_set(self):
        # given
        with pytest.raises(TypeError):
            # when
            ExistingFolderPath(path=123)
            # then

    def test_raise_when_path_of_file_is_set(self):
        # given
        with pytest.raises(ValueError):
            # when
            ExistingFolderPath(path=str(SELF_FILE))
            # then


class TestGlob:
    """Test ExistingFolderPath.glob"""
    def test_gets_directories_and_files(self):
        # given
        src = ExistingFolderPath(path=str(SOURCE_FOLDER))
        # when
        path_list = src.glob(pattern='**/*')
        # then
        assert sorted([p.name for p in path_list]) == sorted([
            'dir01',
            'dir01-01',
            '.gitignore',
            'file01-01.txt',
            'md01-01.md',
            'file01.txt',
            'index.md'
        ])


class TestCopy:
    """Test ExistingFolderPath.copy"""
    def test_copies_directories_and_files(self, tmp_path):
        # given
        src = ExistingFolderPath(path=str(SOURCE_FOLDER))
        dst = NotExistingFolderPath(path=str(tmp_path.joinpath('dst').resolve()))
        assert [p for p in dst.pathlib_path().glob(pattern='**/*')] == []
        # when
        src.copy(destination=dst)
        # then
        name_list = [p.name for p in dst.pathlib_path().glob(pattern='**/*')]
        assert sorted(name_list) == sorted([
            'dir01',
            'dir01-01',
            '.gitignore',
            'file01-01.txt',
            'md01-01.md',
            'file01.txt',
            'index.md'
        ])
