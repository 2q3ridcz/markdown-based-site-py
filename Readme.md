# markdown-based-site-py

Create html based site content from markdown based site content.

## GitLab Pages

Check the sample output at [GitLab Pages](https://2q3ridcz.gitlab.io/markdown-based-site-py/)

## usage

Executable sample is in console_publish folder.

See [./console_publish/Readme.md](./console_publish/Readme.md) for detail.
