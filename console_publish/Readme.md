# markdown-based-site-py

## console_publish

Create html based site content from markdown based site content.

### usage

To check the sample output:

1. In shell console, set current directory to console_publish (the folder this Readme.md is in) and execute this command.
    ```shell
    python ./publish_main.py
    ```
2. Check the output in ./Output/dest folder.

To parse the contents of your own:

1. Place markdown based site contents under ./Input/source folder.
2. Edit jinja2 template file in ./Input/templates/template.j2.html folder.
3. In shell console, set current directory to console_publish (the folder this Readme.md is in) and execute this command.
    ```shell
    python ./publish_main.py
    ```
4. Check the output in ./Output/dest folder.

### features

- [x] Copy all files in ./Input/source folder to ./Output/dest folder.
- [x] Convert html files from ./Output/dest/**/*.md files using template.j2.html.
- [x] Relative links to md files are overwritten to point converted html files.
- [ ] Use relative path for css and javascript in template.j2.html
