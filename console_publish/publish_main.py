import pathlib
import sys

SELF_FILE = pathlib.Path(__file__).resolve()
PKG_FOLDER = SELF_FILE.parent.parent.resolve()
if str(PKG_FOLDER) not in sys.path:
    sys.path.append(str(PKG_FOLDER))

from markdown_based_site.markdown_based_site import MarkdownBasedSite


if __name__ == '__main__':
    input_folder = SELF_FILE.parent.joinpath('Input').resolve()
    output_folder = SELF_FILE.parent.joinpath('Output').resolve()

    SOURCE_FOLDER = input_folder.joinpath('source')
    DEST_FOLDER = output_folder.joinpath('dest')
    MD_TEMPLATE_FILE = input_folder.joinpath('templates/template.j2.html')

    site = MarkdownBasedSite(
        source_folder=str(SOURCE_FOLDER),
        dest_folder=str(DEST_FOLDER),
        md_template_file=str(MD_TEMPLATE_FILE),
    )
    site.publish()
