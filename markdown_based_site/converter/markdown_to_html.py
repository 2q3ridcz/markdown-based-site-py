import dataclasses

import jinja2
import pypandoc
from ..pathlib.existing_file_path import ExistingFilePath


@dataclasses.dataclass(frozen=True)
class MarkdownToHtml():
    """Class to convert markdown string to html string

    Attributes:
        text (str): Markdown string.
        title (str): String to set in title tag in html.
        template_file (ExistingFilePath): Path of a jinja2 template file.
    """
    text: str
    title: str
    template_file: ExistingFilePath

    def convert(self) -> str:
        """Converts markdown to html using marked.js

        Markdown is written inside html and will be parsed by marked.js.
        """
        title = self.title
        text = self.text
        template_file = self.template_file.pathlib_path()

        html = pypandoc.convert_text(text, to='html', format='gfm')

        data = {
            'title': title,
            'content': html,
            'mermaid_flg': (
                False if text.find(r'```mermaid') == -1 else True
            ),
        }

        template_parent = template_file.parent.resolve()
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(str(template_parent))
        )
        template = env.get_template(template_file.name)
        rendered = template.render(data=data)
        return rendered
