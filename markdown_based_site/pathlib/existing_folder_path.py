import dataclasses
import pathlib
import shutil
import typing

from .not_existing_folder_path import NotExistingFolderPath


@dataclasses.dataclass(frozen=True)
class ExistingFolderPath:
    """Path of an existing folder.

    Checks existence when the instance is created.
    Does not know if the file is deleted afterwards.

    Raises:
        TypeError: Not a valid path.
        ValueError: Folder does not exist.
    """
    path: str

    def __post_init__(self):
        selfpath = self.pathlib_path()
        if not selfpath.is_dir():
            raise ValueError('Folder does not exist.')

    def pathlib_path(self) -> pathlib.Path:
        """Pathlib.Path object of the path.

        Returns:
            pathlib.Path: Object of the path.

        Raises:
            TypeError: Not a valid path.
        """
        return pathlib.Path(self.path).resolve()

    def glob(self, pattern: str) -> typing.List[pathlib.Path]:
        """Search for the folder's children (folders or files).

        Iterate over this subtree and yield all existing files (of any
        kind, including directories) matching the given relative pattern.

        Args:
            pattern (str): Search condition

        Returns:
            typing.List[pathlib.Path]:
                Path of folders or files that met the pattern.
        """
        folder = self.pathlib_path()
        return [p for p in folder.glob(pattern=pattern)]

    def copy(self, destination: NotExistingFolderPath) -> 'ExistingFolderPath':
        """Copy the folder's children (folders or files) to destination folder.

        Args:
            destination (NotExistingFolderPath): A folder to copy children to.

        Returns:
            ExistingFolderPath:
                Path of destination folder.
        """
        src = str(self.pathlib_path())
        dst = str(destination.pathlib_path())
        shutil.copytree(src=src, dst=dst)
        return ExistingFolderPath(path=dst)
