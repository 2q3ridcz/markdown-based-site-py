import dataclasses
import pathlib


@dataclasses.dataclass(frozen=True)
class ExistingFilePath:
    """Path of an existing file.

    Checks existence when the instance is created.
    Does not know if the file is deleted afterwards.

    Raises:
        TypeError: Not a valid path.
        ValueError: File does not exist.
    """
    path: str

    def __post_init__(self):
        selfpath = self.pathlib_path()
        if not selfpath.is_file():
            raise ValueError('File does not exist.')

    def pathlib_path(self) -> pathlib.Path:
        """Pathlib.Path object of the path.

        Returns:
            pathlib.Path: Object of the path.

        Raises:
            TypeError: Not a valid path.
        """
        return pathlib.Path(self.path).resolve()
