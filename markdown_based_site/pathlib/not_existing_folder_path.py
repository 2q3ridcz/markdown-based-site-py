import dataclasses
import pathlib


@dataclasses.dataclass(frozen=True)
class NotExistingFolderPath:
    """Path of a non-existing folder, but its parent exists.

    Checks existence when the instance is created.
    Does not know if the file is added / deleted afterwards.

    Raises:
        TypeError: Not a valid path.
        ValueError: Folder exists.
        ValueError: Parent does not exist.
    """
    path: str

    def __post_init__(self):
        selfpath = self.pathlib_path()
        if selfpath.exists():
            raise ValueError('Folder exists.')
        if not selfpath.parent.is_dir():
            raise ValueError('Parent does not exist.')

    def pathlib_path(self) -> pathlib.Path:
        """Pathlib.Path object of the path.

        Returns:
            pathlib.Path: Object of the path.

        Raises:
            TypeError: Not a valid path.
        """
        return pathlib.Path(self.path).resolve()
