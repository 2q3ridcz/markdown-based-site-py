import dataclasses

from .md_processor import MdProcessor
from .pathlib.existing_folder_path import ExistingFolderPath
from .pathlib.not_existing_folder_path import NotExistingFolderPath
from .pathlib.existing_file_path import ExistingFilePath


@dataclasses.dataclass(frozen=True)
class MarkdownBasedSite:
    """Class to parse markdown based site content.

    Attributes:
        source_folder (str):
            Path of input folder. Markdown based site contents are placed.

        dest_folder (str):
            Path of output folder. Html based site contents will be placed.

        md_template_file (str): Path to markdown template file.

    Raises:
        TypeError: source_folder is not a valid path.
        ValueError: source_folder is not a folder.

        TypeError: dest_folder is not a valid path.
        ValueError: dest_folder already exists.
        ValueError: parent of dest_folder is not a folder.

        TypeError: md_template_file is not a valid path.
        ValueError: md_template_file is not a file.
    """
    source_folder: str
    dest_folder: str
    md_template_file: str

    def __post_init__(self):
        # Create objects to validate attributes.
        self._source_folder_pathlib()
        self._dest_folder_pathlib()
        self._md_template_file_pathlib()

    def _source_folder_pathlib(self) -> ExistingFolderPath:
        return ExistingFolderPath(path=self.source_folder)

    def _dest_folder_pathlib(self) -> NotExistingFolderPath:
        return NotExistingFolderPath(path=self.dest_folder)

    def _md_template_file_pathlib(self) -> ExistingFilePath:
        return ExistingFilePath(path=self.md_template_file)

    def publish(self) -> None:
        """Create html based site content from markdown based site content.

        Copies all files in source_folder to dest_folder.
        Markdown files are converted to html file using md_template_file.
        """
        source_folder = self._source_folder_pathlib()
        dest_folder = self._dest_folder_pathlib()
        md_template_file = self._md_template_file_pathlib()

        dst = source_folder.copy(destination=dest_folder)

        md_file_list = dst.glob(pattern='**/*.md')
        for file in md_file_list:
            if file.suffix in MdProcessor.target_suffixes():
                processor = MdProcessor(
                    path=ExistingFilePath(path=str(file.resolve())),
                    md_template_file=md_template_file,
                )
                processor.execute()
