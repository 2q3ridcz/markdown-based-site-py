import dataclasses
import typing

from .pathlib.existing_file_path import ExistingFilePath
from .converter.markdown_to_html import MarkdownToHtml
from .editor.html_editor import HtmlEditor


@dataclasses.dataclass(frozen=True)
class MdProcessor():
    """Class to process markdown file

    Attributes:
        path (str): Path of a markdown file.
        md_template_file (ExistingFilePath): Path of a markdown template file.

    Raises:
        TypeError: path is not a valid path.
        ValueError: path is not a file.

        TypeError: md_template_file is not a valid path.
        ValueError: md_template_file is not a file.
    """
    path: ExistingFilePath
    md_template_file: ExistingFilePath

    @staticmethod
    def target_suffixes() -> typing.List[str]:
        return ['.md']

    def execute(self) -> None:
        """Process markdown file

        Convert markdown file to html file using jinja2 template.
        """
        file = self.path.pathlib_path()
        md_template_file = self.md_template_file

        out_file = file.parent.joinpath(file.stem + '.html')
        md_text = file.read_text(encoding='utf-8')

        converter = MarkdownToHtml(
            text=md_text,
            title=file.stem,
            template_file=md_template_file,
        )
        html = converter.convert()
        editor = HtmlEditor(html=html)
        editted_html = editor.edit()
        with open(str(out_file.resolve()), 'w', encoding='utf-8') as f:
            f.write(editted_html)
