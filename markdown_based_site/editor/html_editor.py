import dataclasses
import urllib
from bs4 import BeautifulSoup


@dataclasses.dataclass(frozen=True)
class HtmlEditor():
    """Class to edit html file

    Attributes:
        html (str): Html string.
    """
    html: str

    def edit(self) -> str:
        """Edit html.

        Operates the instance's `overwrite_relative_md_link` method.
        """
        html = self.overwrite_relative_md_link()
        return html

    def overwrite_relative_md_link(self) -> str:
        """Overwrite relative links to md files to point converted html files.

        If anchor tag's href is a relative url pointing to a markdown file,
        this function overwrites its extension to '.html'.
        If there are no such anchor tags, output is the same as input.

        Returns:
            str: Html string.
        """
        html = self.html

        # overwrite relative .md links to .html.
        soup = BeautifulSoup(markup=html, features="html.parser")
        elms = soup.find_all(name='a')
        for elm in elms:
            href = elm.get('href')
            if href is None:
                continue

            parsed_url = urllib.parse.urlparse(href)
            is_relative = parsed_url.netloc == ''
            is_markdown = parsed_url.path.endswith('.md')
            if not (is_relative & is_markdown):
                continue

            # overwrite
            splitted_path = parsed_url.path.split('.')
            splitted_path[-1] = 'html'
            joined_path = ".".join(splitted_path)
            new_parsed_url = parsed_url._replace(path=joined_path)
            new_href = urllib.parse.urlunparse(new_parsed_url)
            elm['href'] = new_href

        return str(soup)
