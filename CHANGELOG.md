# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.3] - 2023-04-13
### Changed
- Pandoc format from 'md' to 'gfm', because 'gfm' seems to be closer to GitLab Flavored Markdown.

## [0.0.2] - 2023-04-09
### Added
- Relative links to md files are overwritten to point converted html files.

### Changed
- Use pandoc to parse markdown. (Does not use marked.js.)

## [0.0.1] - 2023-03-22
### Added
- GitLab Pages
- MarkdownBasedSite

[Unreleased]: https://gitlab.com/2q3ridcz/markdown-based-site-py/-/compare/v0.0.3...main
[0.0.3]: https://gitlab.com/2q3ridcz/markdown-based-site-py/-/compare/v0.0.2...v0.0.3
[0.0.2]: https://gitlab.com/2q3ridcz/markdown-based-site-py/-/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/2q3ridcz/markdown-based-site-py/-/tree/v0.0.1
